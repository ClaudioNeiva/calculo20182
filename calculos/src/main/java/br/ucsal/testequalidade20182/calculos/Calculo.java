package br.ucsal.testequalidade20182.calculos;

public class Calculo {

	/**
	 * Calcula o percentual de desconto para compra de produtos por tipo.
	 * Produtos de lazer não tem desconto. Produtos de saúde tem desconto de 10%
	 * para compras acima 50 unidade e 15% acima 200. Produtos de alimentação em
	 * 5% nas compras acima de 100 unidades, 10% nas compras acima de 300
	 * unidades e 16% na compras acima de 1000 unidades.
	 * 
	 * @param quantidade
	 * @return percentual de desconto
	 */
	public static Long calcularPercentualDesconto(Long quantidade, TipoProdutoEnum tipo) {
		Long percentualDesconto = 0L;
		if (TipoProdutoEnum.SAUDE.equals(tipo)) {
			if (quantidade > 50) {
				percentualDesconto = 10L;
			} else if (quantidade > 200) {
				percentualDesconto = 15L;
			}
		} else if (TipoProdutoEnum.ALIMENTACAO.equals(tipo)) {
			if (quantidade > 100) {
				percentualDesconto = 5L;
			} else if (quantidade < 300) {
				percentualDesconto = 10L;
			} else if (quantidade > 1000) {
				percentualDesconto = 16L;
			}
		}
		return percentualDesconto;
	}

}
